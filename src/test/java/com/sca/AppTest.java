package com.sca;


import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class AppTest {

    @Test
    public void testMaximalSequence() {
        String input = "AAzzAAppAA";
        char[] chars = input.toCharArray();
        Map<Character, Integer> map = new HashMap<>();
        for (char aChar : chars) {
            Integer charCount = map.putIfAbsent(aChar, 1);
            if (charCount != null) {
                charCount++;
                map.put(aChar, charCount);
            }
        }
        Optional<Integer> max = map.values().stream().max(Integer::compareTo);
        // can be optimized with
        //Optional<Integer> max = map.values().stream().max(Comparator.naturalOrder());
        assertTrue(max.isPresent());
        assertThat(max.get(), equalTo(6));
    }
}
